'use strict';

const fs = require('fs');
const Discord = require('discord.js');
const dotenv = require('dotenv');
const Nightmare = require('nightmare');
const CronJob = require('cron').CronJob;
const ip = require("ip");
const https = require('https')
const qs = require('querystring');
const portfinder = require('portfinder');
const rn = require('random-number');

dotenv.config();
const token = process.env.TOKEN;
const url = process.env.URL;
const newswire = process.env.NEWSWIRE;
const channelID = process.env.NEWS_CHANNEL;
const botChannelId = process.env.BOT_CHANNEL;
const postToken = process.env.POST_TOKEN;
const port = process.env.PORT;
const admin = process.env.ADMIN;


const client = new Discord.Client();
client.login(token);

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setActivity("Grand Theft Auto V");
});


// Every Thursday at 13:36:55
var job = new CronJob('55 36 13 * * 4', async function () {

  /*
  portfinder.getPort(function (err, port) {
    console.log(`${ip.address()}:${port}`)
  });
  */

  console.log(`Cron job started: ${new Date}`);
  client.users.cache.get(admin).send(`Cron job started`);
  let gtaEvents = await getRequest(url);

  // Try to get new and live gta events
  let fields = [];
  while (!fields.length) {
    for (let event of gtaEvents.events) {
      if (event.isLive) {
        fields.push({ name: ":star: " + event.eventName, value: event.description });
      }
    }

    // If there are no new events, wait one hour and try again
    if (!fields.length) {
      sleep((10000 * 60) * 60).then(() => {
        console.log("No new events, waiting another hour");
        client.users.cache.get(admin).send(`No new events, waiting another hour`);
      });
    }
  }
  console.log(`Retrieved ${fields.length} new live events`);

  // TODO : Get the rest of the information also

  client.users.cache.get(admin).send(`Retrieved ${fields.length} new live events`);


}, null, true, 'Europe/Oslo');
job.start();


async function getUpdate(channel) {
  let gtaEvents = await getRequest(url);
  //let newswireURL = await getNewswireURL();
  let details = await getPodiumDetails();

  let fields = [];
  for (let event of gtaEvents.events) {
    if (event.isLive) {
      fields.push({ name: ":star: " + event.eventName, value: event.description });
    }
  }

  console.log(fields);

  const mainEmbed = new Discord.MessageEmbed()
    .setColor('47761E')
    .setTitle(details._mainTitle)
    .setURL(details._mainURL)
    .setDescription(details._mainDesc)
    .addFields(fields)
    .setImage(details._mainImage)
    .setTimestamp()
    .setFooter('Lester Crest', 'https://vignette.wikia.nocookie.net/gtawiki/images/3/36/Lester_photo_nextgen.png/revision/latest?cb=20141220210454');

  const podiumEmbed = new Discord.MessageEmbed()
    .setColor('61B5CB')
    .setTitle(details._podiumTitle)
    .setURL(details._mainURL)
    .setDescription(details._poddiumDesc)
    .setImage(details._podiumImage)
    .setTimestamp()
    .setFooter('Lester Crest', 'https://vignette.wikia.nocookie.net/gtawiki/images/3/36/Lester_photo_nextgen.png/revision/latest?cb=20141220210454');

  channel.send(mainEmbed);
  channel.send(podiumEmbed);
}

async function getNewswireURL() {
  let json = await getRequest(newswire);

  for (let post of json.posts) {
    if (null !== post.expired_after && post.blurb.includes('GTA')) {
      console.log("Potential GTA body:");
      console.log(post);

      let nUrl = `https://www.rockstargames.com/newswire/get-article/${post.id}.json`
      console.log(nUrl);
      return nUrl;
    }
  }
}

async function getPodiumDetails(jsonURL) {

  let json = await getRequest(jsonURL);


  let variables = json.data.post.posts_jsx.variables_us_defaulted;
  let details = new Details(json.data.post.title, variables.url + variables.header_img, variables.header_desc1 + "\n" + variables.header_desc2, json.meta.url, variables.url + variables.podium_img, variables.podium, variables.podium_desc);
  console.log(details);
  return details;

}

async function getRequest(url) {
  console.log(`Get request to: ${url}`);
  const nightmare = Nightmare();
  let _response;
  await nightmare
    .goto(url)
    .wait('body')
    .evaluate(() => document.querySelector('body').innerText)
    .end()
    .then(response => {
      _response = response;
    }).catch(err => {
      console.log(err);
    });

  return JSON.parse(_response);
}

function getFile(fileName) {
  console.log(`Getting JSON from file: ${fileName}`);
  let filedata = fs.readFileSync(fileName);
  return JSON.parse(filedata);
}

/* TODO : add this
// Set up hardcoded responses from json file
let rawdata = fs.readFileSync('lines.json');
var lines = JSON.parse(rawdata);
for (let line of lines) {
  client.on('message', msg => {
    if (msg.content === line.message) {
      console.log(msg.author.tag + ": " + msg.content);
      msg.channel.send(line.response);
    }
  });
}

// Ping with ms time
client.on("message", (message) => {
  if (message.content == "!ping") {
    console.log(message.author.tag + ": " + message.content);
    message.channel.send("Pinging ...")
      .then((msg) => {
        let time = (Date.now() - msg.createdTimestamp);
        msg.edit(`Ping: *${time}* ms`);
        console.log(`${time} ms`);
      });
  }
});
*/

// Let admin write message as Lester
client.on("message", (message) => {
  if (message.channel.type == "dm") {

    if (isNotBot(message))
      console.log(`Recieved DM from ${(message.author.id === admin) ? "[ADMIN]" : "[USER]"}${message.author.username}#${message.author.discriminator}: ${message.content}`);

    if (message.author.id === admin) {
      // TODO : make it possible to send to multiple channels with emoji reaction?
      let channel = client.channels.cache.get(channelID);
      message.channel.send("Sending message\n\n" + message.content + "\n\nto #" + channel.name + ", " + channel.guild.name);
      channel.send(message.content);
      console.log(`Message sent ${client.user.tag}[#${channel.name}] "${message.content}"`)

    } else if (isNotBot(message)) {
      sendRandomMessage(message);
    }
  }
});

function isNotBot(message) {
  return message.author.id !== client.user.id;
}

function sendRandomMessage(message) {
  let userName = message.author.username;

  let array = [
    `Hello there, ${userName}!:cowboy:`,
    `Greetings, ${userName}!:hand_splayed:`,
    `Sup, ${userName}?:sunglasses:`,
    `Why so serious, ${userName}?:clown:`,
    `Here’s ${userName}!:knife:`,
    `Hasta La Vista, ${userName}!:gun:`,
    `Yo, ${userName}!:eyes:`
  ];

  var options = {
    min: 0,
    max: array.length - 1,
    integer: true
  }

  message.channel.send(array[rn(options)]);
}

class Details {
  constructor(mainTitle, mainImage, mainDesc, mainURL, podiumImage, podiumTitle, podiumDesc) {
    this._mainTitle = mainTitle;
    this._mainImage = mainImage;
    this._mainDesc = mainDesc;
    this._mainURL = mainURL;
    this._podiumImage = podiumImage;
    this._podiumTitle = podiumTitle;
    this._poddiumDesc = podiumDesc;
  }
}
